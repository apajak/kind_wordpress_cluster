# kind_wordpress_cluster (Floa training)

## Description

This is a simple example of a Kubernetes cluster with a Wordpress application, Nginx as Revers Proxy and a MySQL 
database.

## Requirements

* [Docker](https://www.docker.com/)
* [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/)
* [Kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
* [K9s](https://k9scli.io/)

## Usage

### Create the cluster

```bash
kind create cluster --config cluster.yaml --name wordpress
```

### Create the database password

```bash
kubectl apply -f secret-generator.yaml
```

### Create the database

```bash
kubectl apply -f mysql/
```

### Create the Wordpress application

```bash
kubectl apply -f wordpress/
```

### Create the Nginx Revers Proxy

```bash
kubectl apply -f nginx/
```

### Access to the application

```bash
kubectl port-forward service/nginx 8080:80
```

Open your browser and go to http://localhost:8080

### Delete the cluster

```bash
kind delete cluster --name wordpress
```

## Author
LexIt

